#ifndef _WIN32
#error Please compile this on windows
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "user32.lib")

#define PORT 9998
#define BUFLEN 64

static const int sdl2win[] = {
    127, 0,  0,  0,  30, 48, 46, 32,  18,  33,  34,  35,  23,  36,  37,  38,
    50,  49, 24, 25, 16, 19, 31, 20,  22,  47,  17,  45,  21,  44,  2,   3,
    4,   5,  6,  7,  8,  9,  10, 11,  28,  1,   14,  15,  57,  12,  13,  26,
    27,  43, 0,  39, 40, 41, 51, 52,  53,  58,  59,  60,  61,  62,  63,  64,
    65,  66, 67, 68, 87, 88, 55, 70,  89,  82,  71,  73,  83,  79,  81,  77,
    75,  80, 72, 69, 0,  0,  74, 78,  0,   0,   0,   0,   0,   76,  0,   0,
    0,   0,  0,  0,  86, 93, 0,  0,   100, 101, 102, 103, 104, 105, 106, 0,
    0,   0,  0,  0,  0,  0,  0,  0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,  0,  0,  0,  0,  0,  115, 112, 125, 121, 123, 0,   0,   0,   0,
    0,   0,  0,  0,  0,  0,  0,  0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,  0,  0,  0,  0,  0,  0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,  0,  0,  0,  0,  0,  0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,  0,  0,  0,  0,  0,  0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,  0,  0,  0,  0,  0,  0,   0,   0,   0,   0,   0,   0,   0,   0,
    29,  42, 56, 91, 0,  54, 0,  92,  0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,  0,  0,  0,  0,  0,  0,   0,   0,   0,   0,   0,   0,   0};

struct Keysym {
    uint32_t scancode;
    uint32_t sym;
    uint16_t mod;
    uint32_t unused;
};

struct KeyboardEvent {
    uint32_t type;
    uint32_t timestamp;
    uint32_t windowID;
    uint8_t state;
    uint8_t repeat;
    uint8_t padding2;
    uint8_t padding3;
    struct Keysym keysym;
};

struct MouseEvent {
    int32_t x;
    int32_t y;
};

int main() {
    ShowWindow(GetConsoleWindow(), SW_HIDE);

    SOCKET s;
    struct sockaddr_in server, si_other;
    int slen, recv_len;
    char buf[BUFLEN];
    WSADATA wsa;

    slen = sizeof(si_other);

    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
        printf("Failed: %d", WSAGetLastError());
        exit(EXIT_FAILURE);
    }

    if ((s = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET) {
        printf("Socket failed: %d", WSAGetLastError());
        exit(EXIT_FAILURE);
    }

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);

    if (bind(s, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR) {
        printf("Failed to bind: %d", WSAGetLastError());
        exit(EXIT_FAILURE);
    }

    while (1) {
        memset(buf, '\0', BUFLEN);

        if ((recv_len = recvfrom(s, buf, BUFLEN, 0,
                                 (struct sockaddr *)&si_other, &slen)) ==
            SOCKET_ERROR) {
            printf("Recv failed: %d\n", WSAGetLastError());
            continue;
        }

        if (buf[0] == '\0') {
            struct MouseEvent *e = (struct MouseEvent *)&buf[1];

            INPUT ip;
            ip.type = INPUT_MOUSE;
            ip.mi.time = 0;
            ip.mi.dx = e->x;
            ip.mi.dy = e->y;
            ip.mi.dwFlags = MOUSEEVENTF_MOVE;
            SendInput(1, &ip, sizeof(INPUT));

        } else if (buf[0] == '\1') {
            INPUT ip;
            ip.type = INPUT_MOUSE;
            ip.mi.time = 0;
            if (buf[2] == 1)
                ip.mi.dwFlags =
                    buf[1] ? MOUSEEVENTF_LEFTDOWN : MOUSEEVENTF_LEFTUP;
            else if (buf[2] == 2)
                ip.mi.dwFlags =
                    buf[1] ? MOUSEEVENTF_MIDDLEDOWN : MOUSEEVENTF_MIDDLEUP;
            else if (buf[2] == 3)
                ip.mi.dwFlags =
                    buf[1] ? MOUSEEVENTF_RIGHTDOWN : MOUSEEVENTF_RIGHTUP;
            else {
                ip.mi.mouseData = buf[2] - 3;
                ip.mi.dwFlags = buf[1] ? MOUSEEVENTF_XDOWN : MOUSEEVENTF_XUP;
            }
            SendInput(1, &ip, sizeof(INPUT));

        } else if (buf[0] == '\2') {
            struct MouseEvent *e = (struct MouseEvent *)&buf[1];

            INPUT ip;
            ip.type = INPUT_MOUSE;
            ip.mi.time = 0;
            // Reasonable approximation of y * 120
            ip.mi.mouseData = e->y << 7;
            ip.mi.dwFlags = MOUSEEVENTF_WHEEL;
            SendInput(1, &ip, sizeof(INPUT));

        } else if (buf[0] == '\3') {
            struct KeyboardEvent *e = (struct KeyboardEvent *)&buf[1];

            INPUT ip;
            ip.type = INPUT_KEYBOARD;
            ip.ki.time = 0;
            if (e->keysym.scancode == 227) {
                ip.ki.wVk = VK_LWIN;
                if (e->state == 1)
                    ip.ki.dwFlags = 0;
                else
                    ip.ki.dwFlags = KEYEVENTF_KEYUP;
            } else {
                ip.ki.wVk = 0;
                ip.ki.wScan = sdl2win[e->keysym.scancode];
                if (e->state == 1)
                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                else
                    ip.ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP;
            }

            SendInput(1, &ip, sizeof(INPUT));
        }
    }

    closesocket(s);
    WSACleanup();

    return 0;
}
