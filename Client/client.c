#include <SDL2/SDL.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>

#define SERVER "192.168.122.32"
#define PORT 9998
#define BUFFLEN 64

SDL_Window* win;

void grab_focus() {
    SDL_SetWindowGrab(win, SDL_TRUE);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_SetRelativeMouseMode(SDL_TRUE);
}

void drop_focus() {
    SDL_SetWindowGrab(win, SDL_FALSE);
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetRelativeMouseMode(SDL_FALSE);
}

int main(int argc, char* argv[]) {
    struct sockaddr_in si_other;
    int s, i, slen = sizeof(si_other);
    char buf[BUFFLEN];
    s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    memset((char*)&si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);
    inet_aton(SERVER, &si_other.sin_addr);

    SDL_Init(SDL_INIT_VIDEO);
    grab_focus();

    win = SDL_CreateWindow("BetterInput", SDL_WINDOWPOS_UNDEFINED,
                           SDL_WINDOWPOS_UNDEFINED, 1280, 720,
                           SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

    SDL_Event e;

    SDL_Surface* surf = SDL_GetWindowSurface(win);
    SDL_FillRect(surf, NULL, SDL_MapRGB(surf->format, 0x00, 0x00, 0x00));
    SDL_UpdateWindowSurface(win);

    while (1) {
        SDL_WaitEvent(&e);
        memset(buf, '\0', BUFFLEN);
        if (e.type == SDL_QUIT) {
            break;
        } else if (e.type == SDL_MOUSEMOTION) {
            buf[0] = '\0';
            memcpy(&buf[1], &e.motion.xrel, sizeof(int32_t));
            memcpy(&buf[1 + sizeof(int32_t)], &e.motion.yrel, sizeof(int32_t));
            sendto(s, buf, BUFFLEN, 0, (struct sockaddr*)&si_other, slen);
        } else if (e.type == SDL_MOUSEBUTTONDOWN ||
                   e.type == SDL_MOUSEBUTTONUP) {
            buf[0] = '\1';
            buf[1] = e.button.state;
            buf[2] = e.button.button;
            sendto(s, buf, BUFFLEN, 0, (struct sockaddr*)&si_other, slen);
        } else if (e.type == SDL_MOUSEWHEEL) {
            buf[0] = '\2';
            memcpy(&buf[1], &e.wheel.x, sizeof(int32_t));
            memcpy(&buf[1 + sizeof(int32_t)], &e.wheel.y, sizeof(int32_t));
            sendto(s, buf, BUFFLEN, 0, (struct sockaddr*)&si_other, slen);
        } else if (e.type == SDL_KEYUP || e.type == SDL_KEYDOWN) {
            buf[0] = '\3';
            memcpy(&buf[1], &e.key, sizeof(SDL_KeyboardEvent));
            sendto(s, buf, BUFFLEN, 0, (struct sockaddr*)&si_other, slen);
        } else if (e.type == SDL_WINDOWEVENT) {
            if (e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                surf = SDL_GetWindowSurface(win);
                SDL_FillRect(surf, NULL,
                             SDL_MapRGB(surf->format, 0x00, 0x00, 0x00));
                SDL_UpdateWindowSurface(win);
            } else if (e.window.event == SDL_WINDOWEVENT_FOCUS_GAINED) {
                grab_focus();
            } else if (e.window.event == SDL_WINDOWEVENT_FOCUS_LOST) {
                drop_focus();
            }
        }
    }

    SDL_DestroyWindow(win);
    SDL_Quit();
    return 0;
}
